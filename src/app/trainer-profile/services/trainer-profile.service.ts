import { Injectable } from '@angular/core';
import { LocalStorageUtil } from '../../shared/utils/local-storage.util';
import { StorageKeys } from '../../shared/enums/storage-keys.enum';

@Injectable({
  providedIn: 'root'
})
export class TrainerProfileService {

  public getTrainerName(): string {
    return LocalStorageUtil.get(StorageKeys.Trainer) || '';
  }

  public getCollection(): void {
    throw new Error('Method TrainerProfileService.getCollection not implemented');
  }

}
