import { Injectable } from '@angular/core';
import { Pokemon } from '../../pokemon-catalogue/models/pokemon.model';
import { LocalStorageUtil } from '../../shared/utils/local-storage.util';
import { StorageKeys } from '../../shared/enums/storage-keys.enum';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TrainerCollectionService {

  private _collection: Pokemon[] = [];

  constructor() {
    this._collection = LocalStorageUtil.get<Pokemon[]>(StorageKeys.TrainerCollection) || [];
  }

  public existsInCollection(id: number): boolean {
    return Boolean(this._collection.find(pokemon => pokemon.id === id));
  }

  public addToCollection(id: number, name: string): void {
    this._collection.push({
      id,
      name,
      url: `${environment.apiBaseUrl}/${id}`,
      image: `${environment.imageBaseUrl}/${id}.png`
    });
    LocalStorageUtil.set<Pokemon[]>(StorageKeys.TrainerCollection, this._collection);
  }

  public removeFromCollection(id: number): void {
    this._collection = this._collection.filter((pokemon: Pokemon) => pokemon.id === id);
    LocalStorageUtil.set<Pokemon[]>(StorageKeys.TrainerCollection, this._collection);
  }

  get collection(): Pokemon[] {
    return this._collection;
  }

}
