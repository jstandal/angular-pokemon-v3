import { Injectable } from '@angular/core';
import { PaginationDirection } from '../../shared/enums/pagination-direction.enum';

@Injectable({
  providedIn: 'root'
})
export class PokemonCataloguePaginationService {

  private _currentPage: number = 1;
  private _pageCount: number = 0;

  setPageCount(count: number, limit: number): void {
    this._pageCount = Math.ceil(count / limit);
  }

  get pageCount(): number {
    return this._pageCount;
  }

  get currentPage(): number {
    return this._currentPage;
  }

  public updatePagination(direction: PaginationDirection): void {
    if (direction === PaginationDirection.Next) {
      this.next();
    } else if (direction === PaginationDirection.Previous) {
      this.previous();
    }
  }

  private next(): void {
    const nextPage = this._currentPage + 1;
    if (nextPage <= this._pageCount) {
      this._currentPage = nextPage;
    }
  }

  private previous(): void {
    const nextPage = this._currentPage - 1;
    if (nextPage >= 0) {
      this._currentPage = nextPage;
    }
  }

}
