import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { PokemonResponse } from '../models/pokemon-response.model';
import { PaginationDirection } from '../../shared/enums/pagination-direction.enum';
import { environment } from '../../../environments/environment';
import { MOCK_POKEMON_RESPONSE } from '../mocks/catalogue.mock';
import { of } from 'rxjs';
import { PokemonCatalogueApiService } from './pokemon-catalogue-api.service';
import { PokemonCataloguePaginationService } from './pokemon-catalogue-pagination.service';
import { PokemonCatalogueCacheService } from './pokemon-catalogue-cache.service';
import { UrlUtil } from '../../shared/utils/url.util';

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _catalogue: Pokemon[] = [];
  private _error: string = '';

  constructor(
    private readonly http: HttpClient,
    private readonly catalogueApiService: PokemonCatalogueApiService,
    private readonly cataloguePaginationService: PokemonCataloguePaginationService,
    private readonly catalogueCacheService: PokemonCatalogueCacheService
  ) {
  }

  public fetchPokemon(direction: PaginationDirection): void {

    const url: string = this.catalogueApiService.createRequestUrl(direction);

    this.cataloguePaginationService.updatePagination(direction);

    this._catalogue = this.catalogueCacheService.getFromCache(
      this.catalogueApiService.offset,
      this.catalogueApiService.offset + this.catalogueApiService.limit
    );

    if (this._catalogue !== undefined && this._catalogue.length !== 0) {
      return;
    }

    const request$ = environment.useMocks ?
      of(MOCK_POKEMON_RESPONSE) :
      this.http.get<PokemonResponse>(url);

    request$.subscribe(
      (response: PokemonResponse) => {
        this.cataloguePaginationService.setPageCount(response.count, this.catalogueApiService.limit);
        const pokemon: Pokemon[] = response.results.map(this.mapIdAndImage);
        this._catalogue = pokemon;
        this.catalogueCacheService.addToCache(pokemon);
      },
      (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    );
  }

  private mapIdAndImage(pokemon: Pokemon): Pokemon {

    const id = UrlUtil.sliceIdFromUrl(pokemon.url);

    return {
      ...pokemon,
      id,
      name: pokemon.name[0].toUpperCase() + pokemon.name.slice(1),
      image: `assets/pokemon/sprites/${id}.png`,
    };
  }

  public initialized(): boolean {
    return this.catalogueApiService.initialized();
  }

  public catalogue(): Pokemon[] {
    return this._catalogue;
  }

  public pageCount(): number {
    return this.cataloguePaginationService.pageCount;
  }

  public currentPage(): number {
    return this.cataloguePaginationService.currentPage;
  }
}
