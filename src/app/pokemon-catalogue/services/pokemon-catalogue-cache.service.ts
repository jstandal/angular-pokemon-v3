import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueCacheService {

  private _catalogueCache: Pokemon[] = [];

  public addToCache(pokemon: Pokemon[]): void {
    this._catalogueCache.push(...pokemon);
  }

  public getFromCache(start: number, end: number): Pokemon[] {
    let cache: Pokemon[] = [];
    if (this._catalogueCache.length > 0 && this._catalogueCache.length > start) {
      cache = this._catalogueCache.slice(start, end);
    }
    return cache;
  }

}
