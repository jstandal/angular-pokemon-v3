import { Component } from '@angular/core';
import { PokemonCatalogueService } from '../../services/pokemon-catalogue.service';
import { PaginationDirection } from '../../../shared/enums/pagination-direction.enum';

@Component({
  selector: 'app-catalogue-footer',
  templateUrl: './pokemon-catalogue-footer.component.html'
})
export class PokemonCatalogueFooterComponent {

  constructor(private readonly catalogueService: PokemonCatalogueService) {
  }

  get pageCount(): number {
    return this.catalogueService.pageCount();
  }

  get currentPage(): number {
    return this.catalogueService.currentPage();
  }

  public onPrevClick(): void {
    this.catalogueService.fetchPokemon(PaginationDirection.Previous);
  }

  public onNextClick(): void {
    this.catalogueService.fetchPokemon(PaginationDirection.Next);
  }
}
