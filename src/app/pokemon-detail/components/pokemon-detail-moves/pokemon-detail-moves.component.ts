import { Component, Input } from '@angular/core';
import { PokemonMove } from '../../models/pokemon-detail.model';

@Component({
  selector: 'app-pokemon-detail-moves',
  templateUrl: './pokemon-detail-moves.component.html',
  styleUrls: [ './pokemon-detail-moves.component.scss' ]
})
export class PokemonDetailMovesComponent {
  @Input() moves: PokemonMove[] = [];
  public minimizeList: boolean = true;

  public toggleMinimizeList(): void {
    this.minimizeList = !this.minimizeList;
  }
}
