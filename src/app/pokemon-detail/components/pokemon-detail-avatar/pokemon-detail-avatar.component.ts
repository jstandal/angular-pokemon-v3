import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-detail-avatar',
  templateUrl: './pokemon-detail-avatar.component.html',
  styleUrls: [ './pokemon-detail-avatar.component.scss' ]
})
export class PokemonDetailAvatarComponent {
  @Input() image: string | undefined;
  @Input() pokemonName: string = '';

}
