import { Component, Input } from '@angular/core';
import { TrainerCollectionService } from '../../../trainer-profile/services/trainer-collection.service';

@Component({
  selector: 'app-pokemon-detail-catch-button',
  templateUrl: './pokemon-detail-catch-button.component.html',
  styleUrls: [ './pokemon-detail-catch-button.component.scss' ]
})
export class PokemonDetailCatchButtonComponent {

  @Input() pokemonId: number = 0;
  @Input() pokemonName: string = '';

  constructor(private readonly collectionService: TrainerCollectionService) {
  }

  public onCatchClick(): void {
    this.collectionService.addToCollection(this.pokemonId, this.pokemonName);
  }

  get existsInCollection(): boolean {
    return this.collectionService.existsInCollection(this.pokemonId);
  }

}
