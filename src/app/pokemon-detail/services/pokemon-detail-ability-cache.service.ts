import { Injectable } from '@angular/core';
import { PokemonDetail } from '../models/pokemon-detail.model';
import { PokemonAbilityDetail } from '../models/pokemon-ability-detail.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailAbilityCacheService {
  private _cacheLimit: number = 50;
  private _pokemonAbilityCache: PokemonAbilityDetail[] = [];

  public addToCache(abilities: PokemonAbilityDetail[]): void {
    abilities.forEach(ability => { // Avoid duplicate abilities in cache.
      if (this.getFromCache(ability.id) === undefined) {
        this._pokemonAbilityCache.push(ability);
      }
    });
    this._pokemonAbilityCache = this._pokemonAbilityCache.slice(0, this._cacheLimit);
  }

  public getFromCache(abilityId: number): PokemonAbilityDetail | undefined {
    return this._pokemonAbilityCache.find((ability: PokemonAbilityDetail) => {
      return ability.id === abilityId;
    });
  }
}
