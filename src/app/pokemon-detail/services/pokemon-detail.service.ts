import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { PokemonAbility, PokemonDetail } from '../models/pokemon-detail.model';
import { of } from 'rxjs';
import { POKEMON_DETAIL_MOCK } from '../mocks/pokemon-detail.mock';
import { PokemonDetailAbilityService } from './pokemon-detail-ability.service';
import { UrlUtil } from '../../shared/utils/url.util';
import { PokemonAbilityDetail } from '../models/pokemon-ability-detail.model';
import { PokemonDetailCacheService } from './pokemon-detail-cache.service';

const API_BASE_URL: string = environment.apiBaseUrl;
const IMAGE_BASE_URL: string = environment.imageBaseUrl;

@Injectable({
  providedIn: 'root'
})
export class PokemonDetailService {
  private _pokemon: PokemonDetail | undefined;
  private _error: string = '';

  constructor(
    private readonly http: HttpClient,
    private readonly pokemonAbilityService: PokemonDetailAbilityService,
    private readonly pokemonDetailCacheService: PokemonDetailCacheService) {
  }

  private createAbilityIdArray(abilities: PokemonAbility[]): number[] {
    return abilities.map(ability => UrlUtil.sliceIdFromUrl(ability.ability.url));
  }

  public fetchPokemon(name: string): void {

    this._pokemon = undefined;

    const pokemonFromCache: PokemonDetail | undefined = this.pokemonDetailCacheService.getFromCache(name);
    let abilityIds: number[] = [];
    const abilitiesFromCache: PokemonAbilityDetail[] = [];
    let foundAllAbilities: boolean = false;

    if (pokemonFromCache !== undefined) {
      abilityIds = this.createAbilityIdArray(pokemonFromCache.abilities);
      abilitiesFromCache.push(...this.pokemonAbilityService.tryFromCache(abilityIds));
      foundAllAbilities = abilityIds.length === abilitiesFromCache.length;
    }

    if (pokemonFromCache !== undefined && foundAllAbilities) {
      this._pokemon = pokemonFromCache;
      this.pokemonAbilityService.fetchAbilities(abilityIds);
      return;
    }

    const request$ = environment.useMocks ?
      of(POKEMON_DETAIL_MOCK) :
      this.http.get<PokemonDetail>(`${API_BASE_URL}/${name}`);

    request$.subscribe(
      (pokemon: PokemonDetail) => {
        this._pokemon = {
          ...pokemon,
          name: pokemon.name[0].toUpperCase() + pokemon.name.slice(1),
          image: `${IMAGE_BASE_URL}/${pokemon.id}.png`
        };

        this.pokemonDetailCacheService.addToCache(this._pokemon);

        const abilityIds: number[] = this.createAbilityIdArray(pokemon.abilities);
        this.pokemonAbilityService.fetchAbilities(abilityIds);

      },
      (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    );
  }

  public pokemon(): PokemonDetail | undefined {
    return this._pokemon;
  }

  public abilities(): PokemonAbilityDetail[] {
    return this.pokemonAbilityService.abilities;
  }
}
