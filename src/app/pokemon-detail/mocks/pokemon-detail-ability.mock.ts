import { PokemonAbilityDetail } from '../models/pokemon-ability-detail.model';

export const POKEMON_DETAIL_ABILITY_MOCK: PokemonAbilityDetail =
  {
    effect_changes: [
      {
        effect_entries: [
          {
            effect: 'Hat außerhalb vom Kampf keinen Effekt.',
            language: {
              name: 'de',
              url: 'https://pokeapi.co/api/v2/language/6/'
            }
          },
          {
            effect: 'Has no overworld effect.',
            language: {
              name: 'en',
              url: 'https://pokeapi.co/api/v2/language/9/'
            }
          }
        ],
        version_group: { name: 'emerald', url: 'https://pokeapi.co/api/v2/version-group/6/' }
      }
    ],
    effect_entries: [ {
      effect: 'Pokémon mit dieser Fähigkeit kann die accuracy nicht gesenkt werden.\n\nDie Fähigkeit verhindert nur direkte Statuswertveränderungen, der Genauigkeitsverlust durch fog wird nicht verhindert, auch nicht das Erhöhen des Fluchtwerts des Gegners. Pokémon mit dieser Fähigkeit kann trotzdem negative accuracy durch heart swap übertragen werden.\n\nAußerhalb vom Kampf: Wenn das erste Pokémon im Team diese Fähigkeit hat, dann hat jede Begegnung mit einem Pokémon das 5 oder mehr Level niedriger ist, eine Chance von 50% übersprungen zu werden.',
      language: { name: 'de', url: 'https://pokeapi.co/api/v2/language/6/' },
      short_effect: 'Verhindert das Absenken der accuracy.'
    }, {
      effect: 'This Pokémon cannot have its accuracy lowered.\n\nThis ability does not prevent any accuracy losses other than stat modifiers, such as the accuracy cut from fog; nor does it prevent other Pokémon\'s evasion from making this Pokémon\'s moves less accurate.  This Pokémon can still be passed negative accuracy modifiers through heart swap.\n\nOverworld: If the first Pokémon in the party has this ability, any random encounter with a Pokémon five or more levels lower than it has a 50% chance of being skipped.',
      language: { name: 'en', url: 'https://pokeapi.co/api/v2/language/9/' },
      short_effect: 'Prevents accuracy from being lowered.'
    } ],
    id: 51,
    name: 'keen-eye'
  };
