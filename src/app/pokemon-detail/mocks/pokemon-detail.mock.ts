import { PokemonDetail } from '../models/pokemon-detail.model';

export const POKEMON_DETAIL_MOCK: PokemonDetail = {
  abilities: [
    {
      ability: {
        name: 'overgrow',
        url: 'https://pokeapi.co/api/v2/ability/65/'
      },
      is_hidden: false,
      slot: 1
    },
    {
      ability: {
        name: 'chlorophyll',
        url: 'https://pokeapi.co/api/v2/ability/34/'
      },
      is_hidden: true,
      slot: 3
    }
  ],
  base_experience: 64,
  height: 7,
  id: 1,
  moves: [
    {
      move: {
        name: 'razor-wind',
        url: 'https://pokeapi.co/api/v2/move/13/'
      },

    },
    {
      move: {
        name: 'swords-dance',
        url: 'https://pokeapi.co/api/v2/move/14/'
      },

    },
    {
      move: {
        name: 'cut',
        url: 'https://pokeapi.co/api/v2/move/15/'
      },

    },
    {
      move: {
        name: 'bind',
        url: 'https://pokeapi.co/api/v2/move/20/'
      },

    },
    {
      move: {
        name: 'vine-whip',
        url: 'https://pokeapi.co/api/v2/move/22/'
      },

    },
    {
      move: {
        name: 'headbutt',
        url: 'https://pokeapi.co/api/v2/move/29/'
      },

    },
    {
      move: {
        name: 'tackle',
        url: 'https://pokeapi.co/api/v2/move/33/'
      },

    },
    {
      move: {
        name: 'body-slam',
        url: 'https://pokeapi.co/api/v2/move/34/'
      },

    },
    {
      move: {
        name: 'take-down',
        url: 'https://pokeapi.co/api/v2/move/36/'
      },

    },
    {
      move: {
        name: 'double-edge',
        url: 'https://pokeapi.co/api/v2/move/38/'
      },

    },
    {
      move: {
        name: 'growl',
        url: 'https://pokeapi.co/api/v2/move/45/'
      },

    },
    {
      move: {
        name: 'strength',
        url: 'https://pokeapi.co/api/v2/move/70/'
      },

    },
    {
      move: {
        name: 'mega-drain',
        url: 'https://pokeapi.co/api/v2/move/72/'
      },

    },
    {
      move: {
        name: 'leech-seed',
        url: 'https://pokeapi.co/api/v2/move/73/'
      },

    },
    {
      move: {
        name: 'growth',
        url: 'https://pokeapi.co/api/v2/move/74/'
      },


    },
    {
      move: {
        name: 'razor-leaf',
        url: 'https://pokeapi.co/api/v2/move/75/'
      },

    },
    {
      move: {
        name: 'solar-beam',
        url: 'https://pokeapi.co/api/v2/move/76/'
      },

    },
    {
      move: {
        name: 'poison-powder',
        url: 'https://pokeapi.co/api/v2/move/77/'
      },

    },
    {
      move: {
        name: 'sleep-powder',
        url: 'https://pokeapi.co/api/v2/move/79/'
      },

    },
    {
      move: {
        name: 'petal-dance',
        url: 'https://pokeapi.co/api/v2/move/80/'
      },

    },
    {
      move: {
        name: 'string-shot',
        url: 'https://pokeapi.co/api/v2/move/81/'
      },

    },
    {
      move: {
        name: 'toxic',
        url: 'https://pokeapi.co/api/v2/move/92/'
      },

    },
    {
      move: {
        name: 'rage',
        url: 'https://pokeapi.co/api/v2/move/99/'
      },

    },
    {
      move: {
        name: 'mimic',
        url: 'https://pokeapi.co/api/v2/move/102/'
      },

    },
    {
      move: {
        name: 'double-team',
        url: 'https://pokeapi.co/api/v2/move/104/'
      },

    },
    {
      move: {
        name: 'defense-curl',
        url: 'https://pokeapi.co/api/v2/move/111/'
      },

    },
    {
      move: {
        name: 'light-screen',
        url: 'https://pokeapi.co/api/v2/move/113/'
      },

    },
    {
      move: {
        name: 'reflect',
        url: 'https://pokeapi.co/api/v2/move/115/'
      },

    },
    {
      move: {
        name: 'bide',
        url: 'https://pokeapi.co/api/v2/move/117/'
      },

    },
    {
      move: {
        name: 'sludge',
        url: 'https://pokeapi.co/api/v2/move/124/'
      },

    },
    {
      move: {
        name: 'skull-bash',
        url: 'https://pokeapi.co/api/v2/move/130/'
      },

    },
    {
      move: {
        name: 'amnesia',
        url: 'https://pokeapi.co/api/v2/move/133/'
      },

    },
    {
      move: {
        name: 'flash',
        url: 'https://pokeapi.co/api/v2/move/148/'
      },

    },
    {
      move: {
        name: 'rest',
        url: 'https://pokeapi.co/api/v2/move/156/'
      },

    },
    {
      move: {
        name: 'substitute',
        url: 'https://pokeapi.co/api/v2/move/164/'
      },

    },
    {
      move: {
        name: 'snore',
        url: 'https://pokeapi.co/api/v2/move/173/'
      },

    },
    {
      move: {
        name: 'curse',
        url: 'https://pokeapi.co/api/v2/move/174/'
      },
    },
    {
      move: {
        name: 'protect',
        url: 'https://pokeapi.co/api/v2/move/182/'
      },
    },
    {
      move: {
        name: 'sludge-bomb',
        url: 'https://pokeapi.co/api/v2/move/188/'
      },
    },
    {
      move: {
        name: 'mud-slap',
        url: 'https://pokeapi.co/api/v2/move/189/'
      }
    },
    {
      move: {
        name: 'giga-drain',
        url: 'https://pokeapi.co/api/v2/move/202/'
      },

    }
  ],
  name: 'bulbasaur',
  order: 1,
  stats: [
    {
      base_stat: 45,
      effort: 0,
      stat: {
        name: 'hp',
        url: 'https://pokeapi.co/api/v2/stat/1/'
      }
    },
    {
      base_stat: 49,
      effort: 0,
      stat: {
        name: 'attack',
        url: 'https://pokeapi.co/api/v2/stat/2/'
      }
    },
    {
      base_stat: 49,
      effort: 0,
      stat: {
        name: 'defense',
        url: 'https://pokeapi.co/api/v2/stat/3/'
      }
    },
    {
      base_stat: 65,
      effort: 1,
      stat: {
        name: 'special-attack',
        url: 'https://pokeapi.co/api/v2/stat/4/'
      }
    },
    {
      base_stat: 65,
      effort: 0,
      stat: {
        name: 'special-defense',
        url: 'https://pokeapi.co/api/v2/stat/5/'
      }
    },
    {
      base_stat: 45,
      effort: 0,
      stat: {
        name: 'speed',
        url: 'https://pokeapi.co/api/v2/stat/6/'
      }
    }
  ],
  types: [
    {
      slot: 1,
      type: {
        name: 'grass',
        url: 'https://pokeapi.co/api/v2/type/12/'
      }
    },
    {
      slot: 2,
      type: {
        name: 'poison',
        url: 'https://pokeapi.co/api/v2/type/4/'
      }
    }
  ],
  weight: 69
};
