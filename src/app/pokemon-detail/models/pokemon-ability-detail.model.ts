import { PokemonMeta } from './pokemon-detail.model';

export interface PokemonAbilityDetail {
  id: number;
  name: string;
  effect_changes: PokemonAbilityEffectChange[];
  effect_entries: PokemonAbilityEffectEntry[];
}

export interface PokemonAbilityEffectEntry {
  effect: string;
  language: PokemonMeta;
  short_effect?: string;
}

export interface PokemonAbilityEffectChange {
  effect_entries: PokemonAbilityEffectEntry[];
  version_group: PokemonMeta;
}
