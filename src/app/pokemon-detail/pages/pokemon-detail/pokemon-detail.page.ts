import { Component, OnInit } from '@angular/core';
import { PokemonDetailService } from '../../services/pokemon-detail.service';
import { ActivatedRoute } from '@angular/router';
import { PokemonDetail } from '../../models/pokemon-detail.model';
import { AppRoutes } from '../../../shared/enums/app-routes.enum';
import { PokemonAbilityDetail } from '../../models/pokemon-ability-detail.model';

@Component({
  selector: 'app-catalogue-detail-page',
  templateUrl: './pokemon-detail.page.html',
  styleUrls: [ 'pokemon-detail.page.scss' ]
})
export class PokemonDetailPage implements OnInit {

  public pokemonName: string = '';
  public backRoute: string = `/${AppRoutes.Catalogue}`;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly pokemonDetailService: PokemonDetailService) {
  }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.route.params.subscribe(({ name }) => {
      this.pokemonName = name;
      this.pokemonDetailService.fetchPokemon(name);
    });
  }

  get pokemon(): PokemonDetail | undefined {
    return this.pokemonDetailService.pokemon();
  }

  get abilities(): PokemonAbilityDetail[] {
    return this.pokemonDetailService.abilities();
  }

  get baseType(): string {
    if (this.pokemonDetailService.pokemon()) {
      const types = this.pokemonDetailService.pokemon()?.types;
      const baseType = types?.slice(0, 1).pop();
      return baseType ? baseType.type.name : '';
    }
    return '';
  }
}
