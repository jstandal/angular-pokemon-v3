import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginPage } from './login/pages/login/login.page';
import { ContainerComponent } from './shared/components/container/container.component';
import { LoginFormComponent } from './login/components/login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { TrainerProfilePage } from './trainer-profile/pages/trainer-profile/trainer-profile.page';
import { IconButtonComponent } from './shared/components/icon-button/icon-button.component';
import { PokemonCataloguePage } from './pokemon-catalogue/pages/pokemon-catalogue/pokemon-catalogue-page.component';
import { MenuButtonsComponent } from './shared/components/menu-buttons/menu-buttons.component';
import { SearchCataloguePage } from './search-catalogue/pages/search-catalogue/search-catalogue.page';
import { HttpClientModule } from '@angular/common/http';
import { PokemonCatalogueItemComponent } from './pokemon-catalogue/components/pokemon-catalogue-item/pokemon-catalogue-item.component';
import {
  PokemonCatalogueFooterComponent
} from './pokemon-catalogue/components/pokemon-catalogue-footer/pokemon-catalogue-footer.component';
import { PokemonDetailPage } from './pokemon-detail/pages/pokemon-detail/pokemon-detail.page';
import { PokemonDetailAvatarComponent } from './pokemon-detail/components/pokemon-detail-avatar/pokemon-detail-avatar.component';
import { PokemonDetailStatsComponent } from './pokemon-detail/components/pokemon-detail-stats/pokemon-detail-stats.component';
import { PokemonDetailMovesComponent } from './pokemon-detail/components/pokemon-detail-moves/pokemon-detail-moves.component';
import { PokemonDetailTypesComponent } from './pokemon-detail/components/pokemon-detail-types/pokemon-detail-types.component';
import { PokemonDetailAbilitiesComponent } from './pokemon-detail/components/pokemon-detail-abilities/pokemon-detail-abilities.component';
import { PokemonDetailCatchButtonComponent } from './pokemon-detail/components/pokemon-detail-catch-button/pokemon-detail-catch-button.component';
import { TrainerCollectionComponent } from './trainer-profile/components/trainer-collected-pokemon/trainer-collection.component';

@NgModule({
  declarations: [
    AppComponent,
    ContainerComponent,
    LoginPage,
    LoginFormComponent,
    TrainerProfilePage,
    IconButtonComponent,
    PokemonCataloguePage,
    MenuButtonsComponent,
    SearchCataloguePage,
    PokemonCatalogueItemComponent,
    PokemonCatalogueFooterComponent,
    PokemonDetailPage,
    PokemonDetailAvatarComponent,
    PokemonDetailStatsComponent,
    PokemonDetailMovesComponent,
    PokemonDetailTypesComponent,
    PokemonDetailAbilitiesComponent,
    PokemonDetailCatchButtonComponent,
    TrainerCollectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
