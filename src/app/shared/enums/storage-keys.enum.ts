export enum StorageKeys {
  Trainer = 'pokemon-trainer',
  TrainerCollection = 'pokemon-collection'
}
