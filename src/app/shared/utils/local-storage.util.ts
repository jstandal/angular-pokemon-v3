export class LocalStorageUtil {
  public static set<T>(key: string, value: T): void {
    try {
      const json: string = JSON.stringify(value);
      const encoded: string = btoa(encodeURIComponent(json));
      localStorage.setItem(key, encoded);
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public static get<T>(key: string): T | null {
    const saved: string | null = localStorage.getItem(key);
    if (!saved) {
      return null;
    }

    try {
      const decoded: string = decodeURIComponent(atob(saved));
      return JSON.parse(decoded) as T;
    } catch (error) {
      return null;
    }
  }

  public static delete(key: string): void {
    localStorage.removeItem(key);
  }

  public static clear(): void {
    localStorage.clear();
  }

}
