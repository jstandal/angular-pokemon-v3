import { Component } from '@angular/core';
import { AppRoutes } from '../../enums/app-routes.enum';

@Component({
  selector: 'app-menu-buttons',
  template: `
    <aside class="menu-buttons d-flex">
      <app-icon-button icon="search" [link]="'/' + routes.SearchCatalogue"></app-icon-button>
      <app-icon-button icon="account_circle" [link]="'/' + routes.TrainerProfile"></app-icon-button>
    </aside>
  `
})
export class MenuButtonsComponent {
  get routes() {
    return AppRoutes;
  }
}
