import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-icon-button',
  template: `
    <button *ngIf="link; else noLinkButton" class="btn icon-only" [routerLink]="link" [ngClass]="btnClass">
      <span class="material-icons">{{icon}}</span>
    </button>
    <ng-template #noLinkButton>
      <button class="btn icon-only"  [ngClass]="btnClass">
        <span class="material-icons">{{icon}}</span>
      </button>
    </ng-template>
  `
})
export class IconButtonComponent {
  @Input() link: string = '';
  @Input() icon: string = 'code';
  @Input() btnClass: string = '';
}
